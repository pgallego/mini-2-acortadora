# Generated by Django 4.1.7 on 2023-03-27 13:19

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('acorta', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='contenido',
            old_name='url',
            new_name='clave',
        ),
        migrations.RenameField(
            model_name='contenido',
            old_name='short',
            new_name='valor',
        ),
    ]
