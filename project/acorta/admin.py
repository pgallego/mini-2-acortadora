from django.contrib import admin
from .models import Contenido, SequentialURL

# Register your models here.
admin.site.register(Contenido)
admin.site.register(SequentialURL)
