from django.db import models


# Create your models here.


class Contenido(models.Model):
    def __str__(self):
        return self.clave

    clave = models.CharField(max_length=64)
    valor = models.TextField()


class SequentialURL(models.Model):
    seq_url = models.IntegerField()
