from django.shortcuts import redirect
from .models import Contenido, SequentialURL
from django.http import HttpResponse
from django.template import loader
from django.views.decorators.csrf import csrf_exempt

seq_short = SequentialURL.objects.last()
# Create your views here.


def redir(request, short):
    target = Contenido.objects.get(valor=short)
    respuesta = redirect(str(target))
    return respuesta


@csrf_exempt
def index(request):
    global seq_short
    if request.method == "POST":
        form_url = request.POST["url"]
        form_short = request.POST["short"]
        exists = Contenido.objects.filter(clave=form_url).exists()
        if not form_short and not exists:
            form_short = str(seq_short.seq_url)
            seq_short.seq_url += 1
            seq_short.save()
        entry = Contenido(clave=form_url, valor=form_short)
        try:
            entry = Contenido.objects.get(clave=form_url)
        except entry.DoesNotExist:
            entry.save()
    content_list = Contenido.objects.all()
    template = loader.get_template('acorta/index.html')
    contexto = {
        'contenido_lista': content_list
    }
    return HttpResponse(template.render(contexto, request))
